import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil
 */
public class InputData {

    /**
     *
     * @param fileName
     * @return
     */
    public List<InputRow> getData(String fileName) {
        List<InputRow> list = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] columns = line.split(",");
                list.add(new InputRow(columns[1], columns[2], Integer.parseInt(columns[3]),
                        Integer.parseInt(columns[4])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    /**
     * Class represents the row of data
     */
    public class InputRow {
        String from;
        String to;
        int start;
        int minutes;

        InputRow(String from, String to, int start, int minutes) {
            this.from = from;
            this.to = to;
            this.minutes = minutes;
            this.start = start;
        }
    }
}
