/**
 * Created by Kamil
 */
public class RailwayRoute {

    /**
     * Target station name
     */
    public String to;

    /**
     * Start time
     */
    public int start;

    /**
     * Travel time in minutes
     */
    public int minutes;

    /**
     * Travel distance
     */
    public int kilometers;

    /**
     * Public constructor
     *
     * @param to      target station name
     * @param start   start time
     * @param minutes travel time in minutes
     */
    public RailwayRoute(String to, int start, int minutes, int kilometers) {
        this.to = to;
        this.start = start;
        this.minutes = minutes;
        this.kilometers = kilometers;
    }

    /**
     * Used to print pretty the Railway Route
     *
     * @return object string to print
     */
    public String getValueToPrint() {
        return "RailwayRoute(to: " + to + ", start: " + start + ", minutes: " + minutes + ")";
    }

}
