import java.util.*;

/**
 * Main class to calculate the problem.
 */
public class Program {

    /**
     * The routs of travel will starts and ends in this city.
     */
    private static final String LIVING_CITY = "Wrocław";

    /**
     * Representation of graph.
     * Key of map represents the start city, and value (List<RailwayRoute>) represent the possible routes.
     */
    private Map<String, List<RailwayRoute>> graph = new TreeMap<>();

    /**
     * Agatka can't travel one route twice - here we save information about it.
     * Key represent starting city, value represents the set of used cities where Agatka travel from city in key.
     */
    private Map<String, Set<String>> usedConnections = new TreeMap<>();

    /**
     * List with solution routes.
     * This will make program faster because we run on single thread, and don't want put reference into method params.
     * After program run it will be empty, because state is dependent of searching state.
     */
    private List<RailwayRoute> currentSolution = new LinkedList<>();

    /**
     * Routes count - we search the biggest value.
     */
    private int bestTravelRoutesCount = -1;

    /**
     * Travel time - when we found the same routes to travel we would choose lower time travel.
     */
    private int bestTravelTime = -1;

    /**
     * The init program method - read the data prom file and
     */
    private void prepareData() {
        graph.clear();
        List<InputData.InputRow> inputRows = new InputData().getData("data.csv");
        for (InputData.InputRow i : inputRows) {
            RailwayRoute connection = new RailwayRoute(i.to, i.start, i.minutes, 0);
            List<RailwayRoute> listToAdd;
            if (graph.containsKey(i.from)) {
                listToAdd = new ArrayList<>();
            } else {
                listToAdd = graph.get(i.from);
            }
            listToAdd.add(connection);
            graph.put(i.from, listToAdd);
        }

        for (String city : graph.keySet()) {
            usedConnections.put(city, new TreeSet<>());
        }
    }

    /**
     * Check that route was travelled
     *
     * @param from start city
     * @param to   target city
     * @return boolean that route was traveled
     */
    private boolean didTraveledByRoute(String from, String to) {
        if (!usedConnections.containsKey(from)) {
            throw new Error("City " + from + " does not exists before");
        }

        return usedConnections.get(from).contains(to);
    }

    /**
     * Method to print the current best solution
     */
    private void printResult() {
        System.out.println("Find better solution!");
        System.out.println("Number of routes: " + currentSolution.size());
        System.out.println("Traveling time: " + bestTravelTime);
        System.out.println("Traveling kilometers: " + bestTravelTime);

        for (int i = 0; i < currentSolution.size(); i++) {
            System.out.println("" + i + ". " + currentSolution.get(i).getValueToPrint());
        }
    }

    /**
     * @param currentRouteSize
     * @param currentTravelTime
     * @return
     */
    boolean checkSolutionIsTheBest(int currentRouteSize, int currentTravelTime) {
        return (currentRouteSize > bestTravelRoutesCount)
                || (currentRouteSize == bestTravelRoutesCount && currentTravelTime <= bestTravelTime);
    }

    /**
     * Main searching method, go in deep through graph and search solutions
     *
     * @param currentCity       current city where method search next routes
     * @param currentTime       time to search routes (can't go back in time)
     * @param currentTravelTime time needed to print solution
     * @param kilometers        total traveled kilometers, needed to print solution
     */
    private void search(String currentCity, int currentTime, int currentTravelTime, int kilometers) {
        if (currentCity.equals(LIVING_CITY)) {
            if (checkSolutionIsTheBest(currentSolution.size(), currentTravelTime)) {
                bestTravelRoutesCount = currentSolution.size();
                bestTravelTime = currentTravelTime;
                printResult();
            }
        }

        for (RailwayRoute route : graph.get(currentCity)) {
            if (!didTraveledByRoute(currentCity, route.to) && currentTime <= route.start) {
                // add to states
                currentSolution.add(route);
                usedConnections.get(currentCity).add(route.to);

                search(route.to, route.start + route.minutes, currentTravelTime + route.minutes,
                        kilometers + route.kilometers);

                // set previous state
                currentSolution.remove(currentSolution.size() - 1);
                usedConnections.get(currentCity).remove(route.to);
            }
        }
    }

    /**
     *
     */
    public void run() {
        prepareData();
        search(LIVING_CITY, 0, 0, 0);
    }

    /**
     * The main method - run it to get the solution.
     *
     * @param args program params, not used
     */
    public static void main(String[] args) {
        new Program().run();
    }

}
